import sun.misc.Unsafe;
import sun.nio.ch.DirectBuffer;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.nio.ByteBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.io.File;
import java.io.RandomAccessFile;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;

import static sun.misc.Unsafe.getUnsafe;

public class basicOffHeap {
    private static String bigTextFile = "RandomList150.txt";

    public static void main(String[] args) throws Exception {

        //Creating a MappedByteBuffer  and writing to it
        int length = 4096;
        try (RandomAccessFile file = new RandomAccessFile("howtodoinjava.dat", "rw")) {
            //create
            MappedByteBuffer out = file.getChannel()
                    .map(FileChannel.MapMode.READ_WRITE, 0, length);
            //update
            for (int i = 0; i < length/2; i++) {
                out.putChar( 'x');
            }


            System.out.println("Finished writing");

            out.position(80);
            out.putChar('y');

            //Read from Mapped Byte Buffer
            // the buffer now reads the file as if it were loaded in memory.
            System.out.println(out.isLoaded());  //prints false
            System.out.println(out.capacity());  //Get the size based on content size of file
            System.out.println(out.isDirect());

            out.rewind();
            //You can read the file from this buffer the way you like.
            for (int i = 0; i < (out.limit()/2); i++) {
                System.out.println(i);
                System.out.print((char) out.getChar()); //Print the content of file
            }
           System.out.println(((DirectBuffer) out).address());
            clean((DirectBuffer) out);

        }


//    public static void main(String[] args) throws IOException {
//        String path = "RandomList150.txt";
//        File file = new File(path);
//        file.createNewFile();
//        FileInputStream fis = new FileInputStream(file);
//        FileChannel readChannel = fis.getChannel();
//        MappedByteBuffer readMap =
//                readChannel.map(FileChannel.MapMode.READ_ONLY,
//                        0,                   // position
//                        readChannel.size()); // size
//
//
//
//    }


    }
    public static boolean clean(DirectBuffer directBuffer){
        try {
            getUnsafe().freeMemory(directBuffer.address());
        } catch (Exception e) {
            System.out.println(e);
        }
        return true;
    }
    public static Unsafe getUnsafe()  {
        Field f = null;
        try {
            f = Unsafe.class.getDeclaredField("theUnsafe");
        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        }
        f.setAccessible(true);
        try {
            return (Unsafe) f.get(null);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }

    }
}
