package com.ukchukx.rocksdbexample;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RocksController {
    @GetMapping("/")
    public String index(){
        System.out.println("Salutations from SpringBoot");
        return "Salutations from SpringBoot";
    }
}
