package com.ukchukx.rocksdbexample.repository;

import java.io.Serializable;
import java.util.Optional;

public interface KVRepository<K, V> extends Serializable {
  boolean save(K key, V value);

  Optional<Object> testFind(String key);

  Optional<V> find(K key);

  boolean delete(K key);
  Optional<V> loadFile(K key);
}