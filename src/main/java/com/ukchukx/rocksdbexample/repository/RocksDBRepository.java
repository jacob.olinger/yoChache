package com.ukchukx.rocksdbexample.repository;

import lombok.extern.slf4j.Slf4j;
import org.rocksdb.Options;
import org.rocksdb.RocksDB;
import org.rocksdb.RocksDBException;
import org.springframework.stereotype.Repository;
import org.springframework.util.SerializationUtils;

import javax.annotation.PostConstruct;
import java.io.*;
import java.lang.management.ManagementFactory;
import java.lang.management.MemoryMXBean;
import java.lang.management.ThreadInfo;
import java.lang.management.ThreadMXBean;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;
import java.util.Scanner;
import java.util.concurrent.Executors;

import static java.util.Optional.empty;
import static org.rocksdb.util.SizeUnit.GB;

@Slf4j
@Repository
public class RocksDBRepository implements KVRepository<String, Object> {
  private final static String FILE_NAME = "spring-boot-db";
  File baseDir;
  static RocksDB db;

  @PostConstruct // execute after the application starts.
  void initialize() {
    RocksDB.loadLibrary();
    Options options = new Options();
    options.optimizeForPointLookup(2000);
//    options.setAllowMmapReads(true);
//    options.setAllowMmapWrites(true);
    log.info(String.valueOf(options.allowMmapWrites()));
    options.setCreateIfMissing(true);
    baseDir = new File("/tmp/rocks", FILE_NAME);

    try {
      Files.createDirectories(baseDir.getParentFile().toPath());
      Files.createDirectories(baseDir.getAbsoluteFile().toPath());
      db = RocksDB.open(options, baseDir.getAbsolutePath());

      log.info("RocksDB initialized");
    } catch(IOException | RocksDBException e) {
      log.error("Error initializng RocksDB. Exception: '{}', message: '{}'", e.getCause(), e.getMessage(), e);
    }
  }

  @Override
  public synchronized boolean save(String key, Object value) {
    log.info("saving value '{}' with key '{}'", value, key);

    try {
      db.put(key.getBytes(), SerializationUtils.serialize(value));
    } catch (RocksDBException e) {
      log.error("Error saving entry. Cause: '{}', message: '{}'", e.getCause(), e.getMessage());

      return false;
    }

    return true;
  }
  @Override
  public synchronized Optional<Object> loadFile(String filename){
    or yo = new or();
    try (java.util.concurrent.ExecutorService executor = Executors.newVirtualThreadPerTaskExecutor()) {
      for (int j = 0; j < 20; j++) {
        int i = j;
        executor.submit(() -> {
          //Thread.sleep(Duration.ofSeconds(1));
          long st = System.nanoTime();
          yo.run();
          long end =System.nanoTime();
          log.info("Thread: " + i + " took " + String.valueOf(end-st));

          return i;
        });
      }

    }  // executor.close() is called implicitly, and waits
    ThreadMXBean threadMXBean = ManagementFactory.getThreadMXBean();

    for(Long threadID : threadMXBean.getAllThreadIds()) {
      ThreadInfo info = threadMXBean.getThreadInfo(threadID);
      log.info("Thread name: " + info.getThreadName());
      log.info("Thread State: " + info.getThreadState());
      log.info("Thread time in blocked state: "+info.getBlockedTime() +" times: " +info.getBlockedCount());
      log.info("Thread is native?: " +info.isInNative());
      log.info(String.format("CPU time: %s ns", threadMXBean.getThreadCpuTime(threadID)));
    }
    return Optional.empty();
  }

  @Override
  public synchronized  Optional<Object> testFind(String key){
    pr yo=new pr();
    Optional<Object> o =Optional.empty();
    MemoryMXBean memoryMXBean = ManagementFactory.getMemoryMXBean();
    long s =System.nanoTime();
    //log.info("Iniial Memory Value is: "+String.valueOf(memoryMXBean.getHeapMemoryUsage().getInit()));
    for(int k=0;k<10000;k++) {
      try (java.util.concurrent.ExecutorService executor = Executors.newVirtualThreadPerTaskExecutor()) {
        for (int j = 0; j < 100; j++) {
          int i = j;
          executor.submit(() -> {
            //Thread.sleep(Duration.ofSeconds(1));
            long st = System.nanoTime();
            yo.run();
            long end = System.nanoTime();
//            log.info("Thread: " + String.valueOf(i) + " Took: " + String.valueOf(end - st));
//            log.info(String.format("Used heap memory: %.2f GB on Thread ", i, " is ", (double) memoryMXBean.getHeapMemoryUsage().getUsed() / 1073741824));
          });
        }

        ThreadMXBean threadMXBean = ManagementFactory.getThreadMXBean();
//
//        for (Long threadID : threadMXBean.getAllThreadIds()) {
//          ThreadInfo info = threadMXBean.getThreadInfo(threadID);
//          log.info("Thread name: " + info.getThreadName());
//          log.info("Thread State: " + info.getThreadState());
//          log.info("Thread time in blocked state: " + info.getBlockedTime() + " times: " + info.getBlockedCount());
//          log.info("Thread is native?: " + info.isInNative());
//          log.info(String.format("CPU time: %s ns", threadMXBean.getThreadCpuTime(threadID)));
//        }

      }  // executor.close() is called implicitly, and waits
    }
    log.info("Final Memory Value is: "+String.valueOf(memoryMXBean.getHeapMemoryUsage().getInit()));
    log.info("Test were: " +(System.nanoTime()-s));

    return o;
  }

  @Override
  public synchronized Optional<Object> find(String key) {
    Object value = null;
    MemoryMXBean memoryMXBean = ManagementFactory.getMemoryMXBean();
      try {
        byte[] bytes = db.get(key.getBytes());
        //this is essentially what the serilization method does
        ByteArrayInputStream by =new ByteArrayInputStream(bytes);
        ObjectInputStream l=new ObjectInputStream(by);
        stor g=new stor();
        g.readObject(l);
       // log.info("Customer since: "+g.customer_Since);
        if (bytes != null) value = SerializationUtils.deserialize(bytes);
        //log.info("Heap Memory Usage is "+ (memoryMXBean.getHeapMemoryUsage().getUsed() /1073741824));
      } catch (Exception e) {
        log.error(
                "Error retrieving the entry with key: {}, cause: {}, message: {}",
                key,
                e.getCause(),
                e.getMessage()
        );
      }
   // log.info("finding key '{}' returns '{}'", key, value);

    return value != null ? Optional.of(value) : empty();
  }

  @Override
  public synchronized boolean delete(String key) {
    log.info("deleting key '{}'", key);

    try {
      db.delete(key.getBytes());
    } catch (RocksDBException e) {
      log.error("Error deleting entry, cause: '{}', message: '{}'", e.getCause(), e.getMessage());

      return false;
    }

    return true;
  }
  class pr implements Runnable{
    final int limit_line=500000;
    Thread op =Thread.startVirtualThread(new Runnable() {
      @Override
      public void run() {
        Object value=null;
        File f = new File("/Users/apple/Desktop/yoChache/Pakistan Largest Ecommerce Dataset [MConverter.eu].txt");
        try {
          Scanner s = new Scanner(f);
          int i=0;
          while(s.hasNextLine()){
            if(i%(limit_line/1000)==0){
              long st=System.nanoTime();
              find(s.nextLine().split(",")[0]);
              //log.info("Time to find index: "+i+ " is: "+(System.nanoTime()-st));

            }else{
              s.nextLine();
            }
            i++;
          }
        } catch (Exception e) {
          log.info("Exception was thrown: " +e);
        }
      }

    });
    @Override
    public void run() {
      op.run();
    }


  }
  public static int getInt(String s){
    int begin=-1;
    if(Character.isDigit(s.charAt(0))){
      begin=0;
    }
    int end=-1;
    for(int i=0;i<s.length();i++){
      if(begin==-1&& Character.isDigit(s.charAt(i))){
        begin=i;
      }
      if(end==-1&& begin!=-1&&!Character.isDigit(s.charAt(i))){
        end=i;
      }
    }
    if(end==-1 && begin==0){
      return Integer.parseInt(s);
    }else if(end==-1){
      return Integer.parseInt(s.substring(begin));
    }
      else {

      return Integer.parseInt(s.substring(begin, end));
    }
  }
  class or implements Runnable{
    final int limit_line=100000;
    Thread op =Thread.startVirtualThread(new Runnable() {
      @Override
      public void run() {
        log.info("saving file '{}'","Pakistan Largest Ecommerce Dataset [MConverter.eu]");
        File f = new File("/Users/apple/Desktop/yoChache/Pakistan Largest Ecommerce Dataset [MConverter.eu].txt");
        long st = System.nanoTime();
        try {
          Scanner s = new Scanner(f);
          int i=0;
          s.nextLine();
          while(s.hasNextLine()){
            String[] val =s.nextLine().split(",");
            if(val.length<20 ){
              continue;
            }
            stor yo =new stor(val);
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(bos);
            oos.writeObject(yo);
            oos.flush();
            byte [] data = bos.toByteArray();
            log.info(String.valueOf(save(val[0],data)));
            log.info((val[0]));
            i++;
          }
        } catch (Exception e) {
          log.error("Error saving entry. Cause: '{}', message: '{}'", e.getCause(), e.getMessage());
          e.printStackTrace();

        }
        long end=System.nanoTime();
        log.info("Time taken to write in all values: "+ String.valueOf(end-st));
      }
    });
    @Override
    public void run() {
      op.run();
    }
    public void start(){

    }

  }

  class stor implements Serializable {
    int id;
    String status;
    Date createdAt;
    String sku;
    double price;
    int qty;
    double total;
    int increment_id;
    String catName;
    String  commisCode;
    double discount;
    String method;
    Date workDate;
    String BI_Status;
    int mv;
    int year;
    int month;
    String customer_Since;
    String m_Y;
    String fY;
    int custer_id;

    public stor(String[] arr){
      int i=0;
      id=getInt(arr[i]);
      i++;
      if(arr[i].length()==0){
        status=null;
        i++;
      }
      else if(arr[i].charAt(0)=='"' && arr[i].charAt(arr[i].length()-1)!='"'){
        status = (arr[i].substring(2) + arr[i+1].substring(0, arr[i+1].length()-2));
        i+=2;
      }else {
        status = arr[i];
        i++;
      }
      createdAt=new Date(arr[i]);
      i++;
      if(arr[i].length()==0){
        sku=null;
        i++;
      }
     else if(arr[i].charAt(0)=='"' &&arr[i].charAt(arr[i].length()-1)!='"' && !arr[i+1].contains("\"") ){
       int j=i;
       sku+=arr[j];
       j++;
       while(!arr[j].contains("\"")) {
         sku+=arr[j];
                 j++;
       }
      sku+=arr[j];
        i=j+1;
      }
      else if(arr[i].charAt(0)=='"' && arr[i].charAt(arr[i].length()-1)!='"'){
        sku = (arr[i].substring(2) + arr[i+1]);
        i+=2;
      }else {
        sku = arr[i];
        i++;
      }
      price=Double.valueOf(arr[i]);
      i++;
      qty=getInt(arr[i]);
      i++;
      total=Double.valueOf(arr[i]);
      i++;
      increment_id=getInt(arr[i]);
      i++;
      if(arr[i].length()==0){
        catName=null;
        i++;
      }
      else if(arr[i].charAt(0)=='"' && arr[i].charAt(arr[i].length()-1)!='"'){
        catName = (arr[i].substring(2) + arr[i+1].substring(0, arr[i+1].length()-2));
        i+=2;
      }else {
        catName = arr[i];
        i++;
      }
      if(arr[i].length()==0){
        commisCode=null;
        i++;
      }
      else if(arr[i].charAt(0)=='"' &&(arr[i].charAt(arr[i].length()-1)!='"' || arr[i].length()==1) && !arr[i+1].contains("\"") ){
        int j=i;
        commisCode+=arr[j];
        j++;
        while(!arr[j].contains("\"")) {
          commisCode+=arr[j];
          j++;
        }
        commisCode+=arr[j];
        i=j+1;
      }
      else if(arr[i].charAt(0)=='"' && (arr[i].charAt(arr[i].length()-1)!='"'|| arr[i].length()==1)){
        commisCode = (arr[i]+ arr[i+1]);
        i+=2;
      }else {
        commisCode = arr[i];
        i++;
      }
      discount=Double.valueOf(arr[i]);
      i++;
      if(arr[i].charAt(0)=='"' && arr[i].charAt(arr[i].length()-1)!='"'){
        method = (arr[i].substring(2) + arr[i+1].substring(0, arr[i+1].length()-2));
        i+=2;
      }else {
        method = arr[i];
        i++;
      }
      workDate=new Date(arr[i]);
      i++;
      if(arr[i].charAt(0)=='"' && arr[i].charAt(arr[i].length()-1)!='"'){
        BI_Status = (arr[i].substring(2) + arr[i+1].substring(0, arr[i+1].length()-2));
        i+=2;
      }else {
        BI_Status= arr[i];
        i++;
      }
      if(arr[i].charAt(0)=='"'){
          mv = Integer.parseInt(arr[i].substring(2) + arr[i+1].substring(0, arr[i+1].length()-2));
          i+=2;
      }else {
        if(arr[i].contains("-")){
          mv=-99;
        }else {
          mv = getInt(arr[i]);
        }
        i++;
        year=getInt(arr[i]);
        i++;
        month=getInt(arr[i]);
        i++;
        customer_Since=arr[i];
        i++;
        m_Y=arr[i];
        i++;
        fY=arr[i];
        i++;
        if(arr[i].equalsIgnoreCase("#N/A")){
          custer_id=-99;
        }else {
          custer_id = getInt(arr[i]);
        }
        i++;
      }


    }
    public stor(int id, String status, Date createdAt, String sku, double price, int qty, double total, int increment_id, String catName, String commisCode, double discount, String method, Date workDate, String BI_Status, int mv, int year, int month, String customer_Since, String m_Y, String fY, int custer_id) {
      this.id = id;
      this.status = status;
      this.createdAt = createdAt;
      this.sku = sku;
      this.price = price;
      this.qty = qty;
      this.total = total;
      this.increment_id = increment_id;
      this.catName = catName;
      this.commisCode = commisCode;
      this.discount = discount;
      this.method = method;
      this.workDate = workDate;
      this.BI_Status = BI_Status;
      this.mv = mv;
      this.year = year;
      this.month = month;
      this.customer_Since = customer_Since;
      this.m_Y = m_Y;
      this.fY = fY;
      this.custer_id = custer_id;
    }

    public stor() {

    }

    public final void readObject(ObjectInputStream in){
      try {
        in.readObject();
      } catch (Exception e) {
        log.error((e.toString()));
        e.printStackTrace();

      }
    }
  }

}

