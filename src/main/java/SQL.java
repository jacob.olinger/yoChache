import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Scanner;

public class SQL {
    public static void main (String[] args) throws Exception {
    //getConnection();

        File file = new File("RandomList150.txt");
        Scanner s = new Scanner(file);
        createTable();
        //System.out.println("Enter in the Names to Be added");
//        while(s.hasNextLine()) {
//            String[] yo = s.nextLine().split(" ");
//            post(yo[0],yo[1]);
//        }
        System.out.println(get());

    }
    public static Connection getConnection()throws Exception{
        try {
            String driver = "com.mysql.cj.jdbc.Driver";
            String url = "jdbc:mysql://localhost: 3306/learn";
            String username = "root";
            String password = "1234";
            Class.forName(driver);
            Connection con = DriverManager.getConnection(url,username,password);
            System.out.println("Connected");
            return con;
        }catch(Exception e){
            System.out.println(e);
        }

        return null;
    }
    public static void createTable() throws Exception{
        try{
            Connection con = getConnection();
            PreparedStatement create =con.prepareStatement("CREATE TABLE IF NOT EXISTS tablename(id int NOT NULL AUTO_INCREMENT, first varchar(255), last varchar(255), PRIMARY KEY(id))");
            create.executeUpdate();
        }catch(Exception e){
            System.out.println(e);
        }finally{
            System.out.println("Function Completed");

        }
    }
    public static void post(String var1,String var2) throws Exception{
        try{
            Connection con = getConnection();
            PreparedStatement posted = con.prepareStatement("INSERT INTO tablename (first, last) VALUES('" +var1 + "','"+var2+"')");
            posted.executeUpdate();
        }catch(Exception e){
            System.out.println(e);
        }finally{
            System.out.println("Sucessful insertion: " + var1 + ", " +var2);
        }
    }

    public static ArrayList<String> get() throws Exception {
        try {
            Connection con = getConnection();
            PreparedStatement statement = con.prepareStatement("SELECT first,last FROM tablename");
            ResultSet result = statement.executeQuery();
            ArrayList<String> arr = new ArrayList<String>();
            while (result.next()) {
                System.out.print(result.getString("first") + " " + result.getString("" + "last") + "\n");
                arr.add(result.getString("last"));
            }
            return arr;

        } catch (Exception e) {
            System.out.println(e);

        } finally {
            System.out.println("All records have been selected");
        }
        return null;

    }

}

