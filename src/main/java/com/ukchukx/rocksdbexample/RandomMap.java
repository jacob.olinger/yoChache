package com.ukchukx.rocksdbexample;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.openjdk.jol.info.GraphLayout;

import java.io.*;
import java.nio.ByteOrder;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.StandardCharsets;
import java.util.LinkedHashMap;
import java.util.Scanner;

public class RandomMap {
    public static void main(String[] args) throws Exception {
        ex exam = new ex(2);
        //parameters
        final int charlen = (int) GraphLayout.parseInstance(exam.yo).totalSize();
        final int intlen =  (int) GraphLayout.parseInstance(exam.id).totalSize();
        final int stringlen = (int) GraphLayout.parseInstance(exam.name).totalSize();

        LinkedHashMap<Integer,Integer> keyStor=new LinkedHashMap<>();
       int length= (int) (GraphLayout.parseInstance(exam.yo).totalSize()+GraphLayout.parseInstance(exam.id).totalSize()+GraphLayout.parseInstance(exam.name).totalSize());
        try(RandomAccessFile file = new RandomAccessFile("howtodoinjava.dat", "rw")) {

            MappedByteBuffer out = file.getChannel().map(FileChannel.MapMode.READ_WRITE, 0, length);
            out.order(ByteOrder.nativeOrder());
            keyStor.put(0,out.position());
            out.putInt(exam.id);
            out.putChar(exam.yo);
            out.put(exam.name.getBytes());
            out.rewind();
            //StandardCharsets.UTF_16.newDecoder().decode(out);

            for(int i=keyStor.get(0)+intlen+charlen;i<keyStor.get(0)+stringlen+charlen+intlen;i+=2){
                System.out.print(out.getChar(i));
            }

        }








//        Scanner s = new Scanner(System.in);
//        ex yo1 = new ex(1);
//        long divider =GraphLayout.parseInstance(yo1).totalSize();
//      byte data[]=new byte[(int) divider];
//        ByteArrayOutputStream bos = new ByteArrayOutputStream();
//        ObjectOutputStream oos = new ObjectOutputStream(bos);
//        oos.writeObject(yo1);
//        oos.flush();
//        data=bos.toByteArray();
//        try(RandomAccessFile file = new RandomAccessFile("howtodoinjava.dat", "rw")) {
//
//            MappedByteBuffer out = file.getChannel().map(FileChannel.MapMode.READ_WRITE, 0, divider*20);
//            out.put(data);
//            byte[] yo3 = new byte[(int) divider*20];
//            out.rewind();
//            out.get(yo3,0, (int) divider*20);
//            try (ByteArrayInputStream b = new ByteArrayInputStream(yo3)) {
//                try (ObjectInputStream o = new ObjectInputStream(b)) {
//                   Object t= o.readObject();
//                    System.out.println(new ObjectMapper().writeValueAsBytes(t));
//                }
//            }
//        }




//            System.out.println(GraphLayout.parseInstance(yo1).toFootprint());
//
//        int length= (int) (256*divider);
//        byte[][]data = new byte[100][(int) GraphLayout.parseInstance(yo1).totalSize()];
//        for(int i=0;i<1;i++) {
//            ex yo = new ex(i);
//
//            ByteArrayOutputStream bos = new ByteArrayOutputStream();
//            ObjectOutputStream oos = new ObjectOutputStream(bos);
//            oos.writeObject(yo);
//            oos.flush();
//            data[i] = bos.toByteArray();
//
//
//
//        }
//        try(RandomAccessFile file = new RandomAccessFile("howtodoinjava.dat", "rw")) {
//
//            MappedByteBuffer out = file.getChannel().map(FileChannel.MapMode.READ_WRITE, 0, length);
//
//            //writing to the Mapped Buffer
//            int i=0;
//            while(i<1){
//                out.put(data[i]);
//                i++;
//            }
//            System.out.println("get the object by i1d 0-99: ");
//            int p=s.nextInt();
//            byte[] yo3 = new byte[(int) divider+8];
//            out.rewind();
//            for(int j = (int) (divider*p); j<(p+1)*divider;j++){
//                yo3[(int) (j-(divider*p))]=out.get(j);
//            }
//           // out.get(yo3,(int) (divider*s.nextInt()), (int) divider);
//            //deserilization
//            try (ByteArrayInputStream b = new ByteArrayInputStream(yo3)) {
//                try (ObjectInputStream o = new ObjectInputStream(b)) {
//                    System.out.println( o.readObject());
//                }
//            }
//
//

//    }
}
 static class ex implements Serializable {
    int id;
    String name;
    char yo;
    public ex(){
        id=2;
        name="Yo";
        yo='O';
    }
    public ex(int i){
        id=i;
        name="Yo";
        yo='L';
    }

}
}

